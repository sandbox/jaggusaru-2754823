(function($, window, Drupal){
	'use strict';
	Drupal.behaviors.gallery={
		attach: function(context, settings){
			$('#dg-container').gallery({
				autoplay	:	true
			});
		}
	};
})(jQuery, window, Drupal);